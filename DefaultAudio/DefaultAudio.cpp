#include <stdio.h>
#include <wchar.h>
#include <tchar.h>
#include <shlwapi.h>
#include "windows.h"
#include "Mmdeviceapi.h"
#include "PolicyConfig.h"
#include "Propidl.h"
#include "Functiondiscoverykeys_devpkey.h"

HRESULT SetDefaultAudioPlaybackDevice(LPCWSTR devID) // Uses an undocumented API.
{
	IPolicyConfigVista* pPolicyConfig;
	ERole reserved = eConsole;

	HRESULT hr = CoCreateInstance(__uuidof(CPolicyConfigVistaClient),
		NULL, CLSCTX_ALL, __uuidof(IPolicyConfigVista), (LPVOID*)&pPolicyConfig);
	if (SUCCEEDED(hr))
	{
		hr = pPolicyConfig->SetDefaultEndpoint(devID, reserved);
		pPolicyConfig->Release();
	}
	return hr;
}

bool isNumber(char number[])
{
	int i = 0;
	if (number[0] == '-') //Is this negative?
	{
		i = 1;
	}
	for (; number[i] != 0; i++) //Itterate through digits.
	{
		if (!isdigit(number[i]))
		{
			return false; //Immediately return false if we find a non-digit.
		}
	}
	return true;
}

// We use unicode since this only works on Vista and above.
int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PWSTR pCmdLine, int nCmdShow)
{
	// Emulate Main() arg stuff
	int argc;
	LPWSTR* argv = CommandLineToArgvW(GetCommandLineW(), &argc);

	// read the command line option, -1 means list devices, this is also the default setting.
	int option = -1;

	if (argc == 2 && isNumber((char*)argv[1]))
	{	
		option = atoi((char*)argv[1]);
	}

	//Setup our message box text.
	char fullpath[MAX_PATH];
	GetModuleFileNameA(nullptr, fullpath, MAX_PATH);
	char text[4096] = "";
	strcat_s(text, "https://bitbucket.org/AshleyStone/defaultaudio/");
	strcat_s(text, "\r\n\r\nUsage: ");
	strcat_s(text, PathFindFileNameA(fullpath));
	strcat_s(text, " [Device #]\r\n\r\nDescription: DefaultAudio sets the default audio device using undocumented Windows APIs in Vista and higher, it will attempt to do silently.");
	strcat_s(text, "\r\n\r\n");

	HRESULT hr = CoInitialize(NULL);
	if (SUCCEEDED(hr))
	{
		IMMDeviceEnumerator* pEnum = NULL;
		// Interface with multimedia device enumerator
		hr = CoCreateInstance(__uuidof(MMDeviceEnumerator), NULL, CLSCTX_ALL, __uuidof(IMMDeviceEnumerator), (void**)&pEnum);
		if (SUCCEEDED(hr))
		{
			IMMDeviceCollection* pDevices;
			// Figure out the output devices.
			hr = pEnum->EnumAudioEndpoints(eRender, DEVICE_STATE_ACTIVE, &pDevices);
			if (SUCCEEDED(hr))
			{
				UINT count;
				pDevices->GetCount(&count);
				if (SUCCEEDED(hr))
				{
					strcat_s(text, "Device list:\r\n");
					for (size_t i = 0; i < count; i++)
					{
						IMMDevice* pDevice;
						hr = pDevices->Item(i, &pDevice);
						if (SUCCEEDED(hr))
						{
							LPWSTR wstrID = NULL;
							hr = pDevice->GetId(&wstrID);
							if (SUCCEEDED(hr))
							{
								IPropertyStore* pStore;
								hr = pDevice->OpenPropertyStore(STGM_READ, &pStore);
								if (SUCCEEDED(hr))
								{
									PROPVARIANT friendlyName;
									PropVariantInit(&friendlyName);
									hr = pStore->GetValue(PKEY_Device_FriendlyName, &friendlyName);
									if (SUCCEEDED(hr))
									{
										if (option == -1) // No arguments or someone set the argument to -1...
										{
											char buff[1024];
											sprintf_s(buff, "%d: %ws\r\n", i, friendlyName.pwszVal);
											strcat_s(text, buff);
										}
										if (i == option)
										{
											SetDefaultAudioPlaybackDevice(wstrID);
										}
										PropVariantClear(&friendlyName);
									}
									pStore->Release();
								}
							}
							pDevice->Release();
						}
					}
					if (option == -1)
					{
						MessageBoxA(NULL, text, "DefaultAudio", MB_OK);
					}
				}
				pDevices->Release();
			}
			pEnum->Release();
		}
		else
		{
			strcat_s(text, "Error: This system does not support or provide access to the undocumented APIs.");
			MessageBoxA(NULL, text, "DefaultAudio", MB_OK);
		}
	}
	return hr;
}